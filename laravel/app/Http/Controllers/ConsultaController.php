<?php

namespace App\Http\Controllers;

use App\Models\Dolar;
use Illuminate\Http\Request;

class ConsultaController extends Controller
{
    public function getTaxa()
    {
        $dolar = new Dolar();
        return response()->json(
            [
                'rate' => $dolar->getValorArredondado(),
                'value' => $dolar->getValor(),
                'date' => date('Y-m-d')
            ]
        );
    }
}
