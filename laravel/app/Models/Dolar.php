<?php

namespace App\Models;

class Dolar
{
    private $ch;
    private $dolar;
    private $url = 'https://boataxa.com.br/Api/Widgets/Template?Moeda=220&Tipo=Comercial';

    public function __construct()
    {
        $this->consulta();
    }

    private function consulta()
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        $html =  curl_exec($this->ch);

        $comprimento = mb_strlen($html);

        $this->dolar = substr($html, ($comprimento - 22),  6);

        curl_close($this->ch);
    }

    private function trataValor()
    {
        $valor = str_replace(',', '.', $this->dolar);

        $valor = (float)$valor;

        $valor = round($valor, 1);

        $valor =  $valor = str_replace(',', '.', $valor);

        return $valor;
    }

    public function getValorArredondado()
    {
        return  $this->trataValor();
    }

    public function getValor()
    {
        return $this->dolar;
    }
}
